# Curriculum Vitae

## Basic information

| **Personal Information** | |
|-----|------|
|Name|Margus Kevin Sünter|
|Date of Birth|24.03.1995|
|Address|Pepleri 3-11 Tartu 51003|
|Phone|+372 533 29 636|
|Email|kevin@synter.ee|


| **Education** | |
|-----|------|
|2016-|**Tallinn Polytechnic Institute** <br> Software developer|
|2011-2014|**Nõo Reaalgümnaasium**|


| **Experience** | |
|-----|------|
|2017-|**Credy** <br /> JS and PHP developer|
|2012-2017|**Freelance** <br /> Have been working mostly with Laravel since 2.0, some C# and little bit of Basic. VueJS since release of 2.0 (2016) |
|2005-2012| Created small webpages for local businesses|


| **Languages** | |
|-----|------|
|Estonian|Native language|
|English|C1|


## Additional information
Have had driving licence for four years.

Fond of programming. While I am not working I constantly learn something new like building some small yet useful projects (like browser addons, native apps etc) just to learn how everything works.

Also interested in cars, robots etc.